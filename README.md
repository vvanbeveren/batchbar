# Threadsafe multiple progressbars for Java

This class allows multiple processes to output their progress to the console in a thread-safe manner. Functionality similar to progress as shown for bitbake's building, or docker's pull.

Though this project supports Ansi, I'm not yet using it to its full potential. For example I'm not using colors. Or nice blocky progress bars. This may be added in the future.

For now this project is inclubating. Many things can change.

Current example output:
```
task-25   | ################################### | 100.0% | Done
task-34   | #########################           |  70.2% | Programming
task-36   | ########################            |  67.2% | Programming
task-40   | #####################               |  59.3% | Programming
task-21   | ##################                  |  50.9% | Programming
task-27   | ####################                |  55.7% | Programming
task-28   | #                                   |   4.0% | Validating
task-24   | ###########################         |  78.5% | Programming
task-29   | ####################                |  56.7% | Programming
task-22   | #############################       |  81.6% | Validating
task-32   | ##                                  |   5.1% | Validating
task-31   | #########################           |  71.6% | Programming
task-37   | #########################           |  71.9% | Programming
task-35   | #########                           |  24.3% | Programming
```

API changes pending. 
