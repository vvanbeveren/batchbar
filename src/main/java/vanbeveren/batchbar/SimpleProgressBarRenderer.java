package vanbeveren.batchbar;

import org.fusesource.jansi.AnsiPrintStream;

import static vanbeveren.batchbar.Sizer.Section;

/**
 * Renders a simple progress bar:
 *
 * <pre>
 *     task | ####             | 25% | state
 * </pre>
 */
public class SimpleProgressBarRenderer implements ProgressBarRenderer {

    private Sizer _sizer;

    public SimpleProgressBarRenderer()
    {
        _sizer = new Sizer(new Section[] {
                new Section(Sizer.Mode.PREFERRED, 0.20f),
                new Section(Sizer.Mode.REQUIRED, 3),
                new Section(Sizer.Mode.MINIMALLY, 0.5f),
                new Section(Sizer.Mode.REQUIRED, 3),
                new Section(Sizer.Mode.REQUIRED, 4),
                new Section(Sizer.Mode.REQUIRED, 3),
                new Section(Sizer.Mode.MINIMALLY, 0.20f),
        });
    }

    private void addSeparator(AnsiPrintStream aps, int len) {
        if (len == 0) return;
        if (len == 1) {
            aps.print('|');
            return;
        }
        int padLeft = len / 2;
        int padRight = len - padLeft - 1;
        aps.print(" ".repeat(padLeft));
        aps.print("|");
        if (padRight > 0) aps.print(" ".repeat(padRight));
    }

    @Override
    public void render(ProgressBar pb, AnsiPrintStream aps, int width) {
        boolean hideState = (pb.getState() == null);
        // hide state if it is not required
        _sizer.getSection(6).setHidden(hideState);
        _sizer.getSection(5).setHidden(hideState);

        if (width < 60) {
            _sizer.getSection(1).setHintSize(1);
            _sizer.getSection(2).setHintSize(0.2f);
            _sizer.getSection(3).setHintSize(1);
            _sizer.getSection(4).setHintSize(4);
            _sizer.getSection(5).setHintSize(1);

        } else {
            _sizer.getSection(1).setHintSize(3);
            _sizer.getSection(2).setHintSize(0.5f);
            _sizer.getSection(3).setHintSize(3);
            _sizer.getSection(4).setHintSize(6);
            _sizer.getSection(5).setHintSize(3);
        }

        int[] sizes = _sizer.updateSizes(width);

        aps.print(Utils.fit(pb.getTaskName(), sizes[0] ));
        addSeparator(aps, sizes[1]);

        if (pb.getMaxProgress() == pb.MAX_PROGRESS_UNDEFINED) {
            aps.print("-".repeat(sizes[2]));
            addSeparator(aps, sizes[3]);
            aps.print(Utils.fit("    ", sizes[4]));
        } else {
            float prog = (float)pb.getProgress() / pb.getMaxProgress();
            if (prog < 0) prog = 0;
            if (prog > 1) prog = 1;
            int progChars = Math.round(sizes[2] * prog);
            if (progChars > 0) {
                aps.print("#".repeat(progChars));
            }
            if (progChars < sizes[2]) {
                aps.print(" ".repeat(sizes[2] - progChars));
            }
            addSeparator(aps, sizes[3]);
            if (sizes[4] >= 6) {
                aps.print(Utils.fit(String.format("%5.1f%%", prog * 100), sizes[4]));
            } else {
                aps.print(Utils.fit(String.format("%3.0f%%", prog * 100), sizes[4]));
            }
        }
        if (!hideState)
        {
            addSeparator(aps, sizes[5]);
            aps.print(Utils.fit(pb.getState(), sizes[6]));
        }
    }
}
