package vanbeveren.batchbar;

import org.fusesource.jansi.AnsiPrintStream;

public class ProgressBar extends LineItemBase {

    /** Set this to maxProgress to set the progressbar to undefined */
    public static final int MAX_PROGRESS_UNDEFINED = 0;

    private final ProgressBarRenderer _renderer;
    private String _taskName;
    private int _progress;
    private int _maxProgress;
    private String _state;


    public ProgressBar(String taskName, int progress, int maxProgress, String state,
                          ProgressBarRenderer renderer, int order) {
        super(order);
        _renderer = renderer;
        _taskName = taskName;
        _state = state;
        _progress = progress;
        _maxProgress = maxProgress;
    }

    public ProgressBarRenderer getRenderer()
    {
        return _renderer;
    }

    @Override
    public void render(AnsiPrintStream aps, int width) {
        _renderer.render(this, aps, width);
    }

    public String getTaskName() {
        return _taskName;
    }

    public void setTaskName(String taskName) {
        _taskName = taskName;
        update();
    }

    public int getProgress() {
        return _progress;
    }

    public void setProgress(int progress) {
        _progress = progress;
        update();
    }

    public void update(int progress, int maxProgress) {
        _progress = progress;
        _maxProgress = maxProgress;
        update();
    }

    public void update(int progress, int maxProgress, String state) {
        _progress = progress;
        _maxProgress = maxProgress;
        _state = state;
        update();
    }

    public int getMaxProgress() {
        return _maxProgress;
    }

    public void setMaxProgress(int maxProgress) {
        _maxProgress = maxProgress;
        update();
    }

    public String getState() {
        return _state;
    }

    public void setState(String state) {
        _state = state;
        update();
    }
}
