package vanbeveren.batchbar;

import static org.fusesource.jansi.Ansi.ansi;

import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.AnsiConsole;
import org.fusesource.jansi.AnsiPrintStream;

import javax.sound.sampled.Line;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BatchBar {

    public final static int DEFAULT_WIDTH = 72;
    public final static int DEFAULT_MAX_UPDATE_RATE = 80;

    private static final BatchBar _BB = new BatchBar();

    private List<LineItem> _lineItems = new ArrayList<>();

    private int _lastLineCount = 0;

    private final AnsiPrintStream _aps;

    private final Thread _render;
    private volatile boolean _isDirty = false;
    private volatile boolean _cancelled = false;    // if this is flagged BatchBar is unusable
    private long _lastUpdate = 0;
    private long _maxUpdateRate = DEFAULT_MAX_UPDATE_RATE;
    private Queue<String> _q = new LinkedList<String>();

    private BatchBar()
    {
        _aps = AnsiConsole.out();
        _render = new Thread(() -> {
            renderLoop();
        }, "batchbar" );
        _render.setDaemon(true);
        _render.start();
    }

    protected static BatchBar singleton()
    {
        return _BB;
    }

    private void insertAtPosition(LineItem li)
    {
        for (int i = _lineItems.size() - 1; i >= 0; i--)
        {
            if (_lineItems.get(i).getOrder() <= li.getOrder()) {
                _lineItems.add(i + 1, li);
                return;
            }
        }
        _lineItems.add(0, li);
    }

    public static void logMessage(String msg) {
        _BB.addMessage(msg);
    }

    private synchronized void addMessage(String msg)
    {
        _q.add(msg);
        _isDirty = true;
        notify();
    }

    protected synchronized void register(LineItem li)
    {
        if (_cancelled) throw new IllegalStateException("Cancelled");
        if (_lineItems.contains(li)) return;
        insertAtPosition(li);
        _isDirty = true;
        notify();
    }

    protected synchronized void unregister(LineItem li)
    {
        if (_cancelled) throw new IllegalStateException("Cancelled");
        if (!_lineItems.contains(li)) return;
        _lineItems.remove(li);
        _isDirty = true;
        notify();
    }

    protected synchronized void update(LineItem li)
    {
        if (_cancelled) throw new IllegalStateException("Cancelled");
        _isDirty = true;
        notify();
    }

    private void update()
    {
        int width = AnsiConsole.getTerminalWidth() - 1;
        if (width == -1) {
            width = DEFAULT_WIDTH;
        }


        if (_lastLineCount > 0) {
            _aps.print(ansi().cursorUp(_lastLineCount).toString());
        }
        while (!_q.isEmpty())
        {
            String msg = _q.remove();
            _aps.print(msg);
            _aps.println(ansi().eraseLine(Ansi.Erase.FORWARD));
        }

        for (LineItem li : _lineItems)
        {
            li.render(_aps, width);
            _aps.println(ansi().eraseLine(Ansi.Erase.FORWARD));
        }

        // erase last N lines if new line-count < prevous line-count
        if (_lastLineCount > _lineItems.size()) {
            for (int i = _lineItems.size(); i < _lastLineCount; i++ )
            {
                _aps.println(ansi().eraseLine(Ansi.Erase.FORWARD));
            }
            _aps.print(ansi().cursorUp(_lastLineCount-_lineItems.size()).toString());
        }
        _lastLineCount = _lineItems.size();
    }

    private void renderLoop() {
        try {
            while (true) {
                synchronized (this) {
                    long now = System.currentTimeMillis();
                    long interval = now - _lastUpdate;
                    // limits the update-rate
                    if (interval >= _maxUpdateRate) {
                        _lastUpdate = now;
                        if (_isDirty) {
                            update();
                            _isDirty = false;
                        }
                        wait();
                    } else {
                        wait(_maxUpdateRate - interval);
                    }
                }
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            _cancelled = true;
        }
    }

    /**
     * Sets the maximum update rate in milliseconds.
     *
     * @see BatchBar.DEFAULT_MAX_UPDATE_RATE
     *
     * @param maxUpdateRate     The maxium update rate in milliseconds
     */
    public synchronized void setMaxUpdateRate(int maxUpdateRate)
    {
        _maxUpdateRate = maxUpdateRate;
    }


    public synchronized void hideAll() {
        LineItem[] liList = _lineItems.toArray(new LineItem[_lineItems.size()]);
        for (LineItem li : liList)
        {
            li.setVisible(false);
        }
    }
}
