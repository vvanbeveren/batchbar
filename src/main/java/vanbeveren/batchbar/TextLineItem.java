package vanbeveren.batchbar;

import org.fusesource.jansi.AnsiPrintStream;

public class TextLineItem extends LineItemBase {

    private String _text;

    public TextLineItem(String initialText, int order) {
        super(order);
        _text = initialText;
    }

    public TextLineItem(String initialText) {
        this(initialText, 0);
    }

    public void setText(String text)
    {
        _text = text;
        update();
    }

    @Override
    public void render(AnsiPrintStream aps, int width) {
        // not even going to try this
        if (width < 3) return;
        if (_text.length() > width) {
            aps.print(_text.substring(0, width-3));
            aps.print("...");
        } else {
            aps.print(_text);
        }
    }
}
