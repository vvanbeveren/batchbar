package vanbeveren.batchbar;

import org.fusesource.jansi.AnsiPrintStream;

public interface ProgressBarRenderer {

    void render(ProgressBar pb, AnsiPrintStream aps, int width);

}
