package vanbeveren.batchbar;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Sizer {

    public enum Mode {
        /* This always requires this size */
        REQUIRED,
        /* This shows a preferred number of chars. But it could be less OR more. */
        PREFERRED,
        /* This has a minimal size, but is spreaded fairly over the remaining space */
        MINIMALLY
    }

    public static class Section {

        private int _chars;
        private float _pcnt;
        private Mode _mode;
        private boolean _hidden = false;
        private int _curSize;

        public Section(Mode mode, int chars) {
            _chars = chars;
            _mode = mode;
            _pcnt = Float.NaN;
        }

        public Section(Mode mode, float pcnt) {
            _pcnt = pcnt;
            _chars = 0;
            _mode = mode;
        }

        public void updateSize(int width) {
            if (isHidden()) {
                _curSize = 0;
            } else {
                if (Float.isNaN(_pcnt)) {
                    _curSize = _chars;
                } else {
                    _curSize = Math.round(width * _pcnt);
                }
                if (_curSize < 0) _curSize = 1;
            }
        }

        public void setHintSize(int chars)
        {
            _chars = chars;
            _pcnt = Float.NaN;
        }

        public void setHintSize(float pcnt)
        {
            _chars = 0;
            _pcnt = pcnt;
        }

        public int getCurSize() {
            return _curSize;
        }

        public void setCurSize(int curSize) {
            _curSize = curSize;
        }

        public Mode getMode()
        {
            return _mode;
        }

        public boolean isHidden() {
            return _hidden;
        }

        public void setHidden(boolean hidden) {
            _hidden = hidden;
        }

        public void changeSize(int delta) {
            _curSize += delta;
        }
    }

    private final Section[] _sections;
    private final int[] _sizes;

    public Sizer(Section[] sections) {
        _sections = sections;
        _sizes = new int[_sections.length];
    }

    private int sizeOf(Stream<Section> strm)
    {
        return strm.mapToInt(Section::getCurSize).sum();
    }

    private int currentSize()
    {
        return sizeOf(Arrays.stream(_sections));
    }

    private void resize(int delta, final Mode which)
    {
        if (delta == 0) return;

        List<Section> sub = Arrays.stream(_sections).filter(
                s -> !s.isHidden() && s.getMode() == which).collect(Collectors.toUnmodifiableList());
        int space = sizeOf(sub.stream());

        // System.out.println("Delta: %d, Mode: %s, Space: %d".formatted(delta, which, space));

        // can't do anything with no space
        if (space == 0) return;

        int[] curChange = new int[sub.size()];

        int realDelta = 0;
        for (int i = 0; i < curChange.length; ++i)
        {
            Section sec = sub.get(i);
            curChange[i] = (int)((float)sec.getCurSize() / space * delta);
            // System.out.println(" -> %d: curSize=%d, curChange=%d".formatted(i, sec.getCurSize(), curChange[i]));
            realDelta += curChange[i];
        }
        int deltaDelta = delta - realDelta;

        // System.out.println(" (in between) realDelta=%d (deltaDelta=%d)".formatted(realDelta, deltaDelta));

        int d;
        if (deltaDelta < 0)
        {
            deltaDelta = -deltaDelta;
            d = -1;
        } else {
            d = 1;
        }
        if (deltaDelta == 1) {
            curChange[curChange.length/2] += d;
        } else {
            for (int i = 0; i < deltaDelta; ++i) {
                int p = (i * (curChange.length - 1)) / (deltaDelta - 1);
                curChange[p] += d;
                // System.out.println(" -> %d: tweak to %d".formatted(p, curChange[p]));
            }
        }

        // System.out.println(" (result): deltaWanted=%d, detlaFound=%d)".formatted(delta, Arrays.stream(curChange).sum()));


        for (int i = 0; i < curChange.length; ++i) {
            Section sec = sub.get(i);
            sec.changeSize(curChange[i]);
        }
    }

    public int[] updateSizes(int width)
    {
        for (Section sec: _sections)
        {
            sec.updateSize(width);
        }

        int delta = width - currentSize();
        if (delta > 0) {
            resize(delta, Mode.MINIMALLY);
            delta = width - currentSize();
        }

        if (delta != 0) {
            resize(delta, Mode.PREFERRED);
            delta = width - currentSize();
            if (delta != 0) {
                resize(delta, Mode.REQUIRED);
            }
        }

        for (int i = 0; i < _sections.length; ++i)
        {
            _sizes[i] = _sections[i].getCurSize();
        }
        return _sizes;
    }

    public Section getSection(int idx)
    {
        return _sections[idx];
    }



}
