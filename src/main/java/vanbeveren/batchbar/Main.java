package vanbeveren.batchbar;


import org.fusesource.jansi.AnsiConsole;

import java.util.Random;

import static org.fusesource.jansi.Ansi.ansi;
import static org.fusesource.jansi.Ansi.Color.*;

public class Main {


    public static class ProgressEmulator extends Thread {

        private Random _rand = new Random();

        public ProgressEmulator()
        {
        }

        @Override
        public void run() {
            try {
                int max = _rand.nextInt(400) + 10;
                ProgressBar pb = new ProgressBar(String.format("task-%d", getId()),
                        0, max, "Programming", new SimpleProgressBarRenderer(), 0);

                Thread.sleep(_rand.nextInt(5000));
                pb.setVisible(true);
                for (int i = 0; i <= max; i++)
                {
                    pb.setProgress(i);
                    Thread.sleep(40);
                }
                Thread.sleep(500);
                max = _rand.nextInt(200) + 5;
                pb.update(0, max, "Validating");
                for (int i = 0; i <= max; i++)
                {
                    pb.setProgress(i);
                    Thread.sleep(40);
                }
                pb.setState("Done");
                Thread.sleep(500);
                pb.setVisible(false);
            } catch (InterruptedException e) {
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 20; i++) {
            new ProgressEmulator().start();
        }


    }
}