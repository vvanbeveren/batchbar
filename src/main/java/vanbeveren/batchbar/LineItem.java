package vanbeveren.batchbar;

import org.fusesource.jansi.AnsiPrintStream;

public interface LineItem {

    /**
     * This value is used to add LineItems to specific positions.
     *
     * If the order if multiple items is the same, the order is the sequence of visibilitiy (setVisible(true))
     *
     * @return
     */
    int getOrder();

    /**
     * Render the line-item
     *
     * @param aps       The ANSI print-stream to write to
     * @param width     The width of the line-item
     */
    void render(AnsiPrintStream aps, int width);

    void setVisible(boolean visible);

    boolean isVisible();

}
