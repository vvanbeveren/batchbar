package vanbeveren.batchbar;

import org.fusesource.jansi.AnsiPrintStream;

public abstract class LineItemBase implements LineItem {

    private boolean _visible = false;
    private int _order;

    protected LineItemBase(int order)
    {
        _order = order;
    }

    public void setVisible(boolean visible)
    {
        synchronized (BatchBar.singleton()) {
            if (!_visible && visible) {
                BatchBar.singleton().register(this);
            } else if (_visible && !visible) {
                BatchBar.singleton().unregister(this);
            }
            _visible = visible;
        }
    }

    protected void update()
    {
        if (_visible) {
            BatchBar.singleton().update(this);
        }
    }

    @Override
    public boolean isVisible() {
        return _visible;
    }

    @Override
    public int getOrder() {
        return _order;
    }
}
