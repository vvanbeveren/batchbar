package vanbeveren.batchbar;

public final class Utils {

    public static String fit(String src, int length) {

        if (length == 0) return "";
        // same.. do nothing
        if (src.length() == length) return src;
        // source smaller, pad with spaces
        if (length > src.length() ) {
            return src + " ".repeat(length - src.length());
        }
        // source larger but length is really small
        if (length <= 3) {
            // This is too small ... show as much as possible
            return src.substring(0, length);
        }

        return src.substring(0, length - 3) + "...";
    }
}
