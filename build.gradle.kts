plugins {
    id("java")
    id("application")
    id("java-library")
    id("distribution")
}

group = "vanbeveren.batchbar"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
    implementation("org.fusesource.jansi:jansi:2.4.0")
}

tasks.test {
    useJUnitPlatform()
}

application {
    mainClass.set("vanbeveren.batchbar.Main")
}

